﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class DragBalls : MonoBehaviour
{
    Rigidbody2D rigidbody2D;
    float force = 100;
    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void OnMouseDown()
    {
        Vector3 mouse =  Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float positionX = mouse.x;
        float positionY = mouse.y;

        rigidbody2D.AddForce(force * new Vector2(positionX,positionY));

    }
}
