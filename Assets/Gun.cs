﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public bool isGunActive;
    private int bulletLeft = 12;

    TurretManager turretManager;
    GameObject bullet;

    private void Awake()
    {
        turretManager = transform.parent.GetComponent<TurretManager>();
        bullet = Resources.Load<GameObject>("bullet");
    }

    private void Update()
    {
        if (isGunActive && IsAnyBulletLeft())
        {
            PreperGun();
        }
    }

    private void Shot()
    {
            bulletLeft--;
            GameObject inst = Instantiate(bullet); //TODO zamienić na object pooling
            inst.transform.position = transform.position;
    }

    private bool IsAnyBulletLeft()
    {
        if (bulletLeft > 0)
        {
            return true;
        }
        turretManager.TurnOff();
        return false;
    } 

    float timeToRotate;
    private void PreperGun()
    {
        timeToRotate -= Time.deltaTime;
        if (timeToRotate <0)
        {
            timeToRotate = 0.5f;
            RotateGun();
            Shot();
        }
    }

    void RotateGun()
    {
        float roattion = Random.Range(15, 45);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + roattion);
    }
}
