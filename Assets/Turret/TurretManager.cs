﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class TurretManager : MonoBehaviour
{
    private float preparationTime = 6;

    [SerializeField]
    private List<SpriteRenderer> colorChangeElements;

    private Gun gun;

    private void Awake()
    {
        gun = transform.GetComponentInChildren<Gun>();    
    }

    private void Update()
    {
        Preaperation();
    }

    private void Preaperation()
    {
        if (preparationTime != 0)
        {
            preparationTime -= Time.deltaTime;
            if (preparationTime < 0)
            {
                preparationTime = 0;
                gun.isGunActive = true;
                ChangeCollor(Color.red);
            }
        }
    }

    private void ChangeCollor(Color color)
    {
        foreach (SpriteRenderer image in colorChangeElements)
        {
            image.color = color;
        }
    }

    public void TurnOff()
    {
        ChangeCollor(Color.white);
    }

}
